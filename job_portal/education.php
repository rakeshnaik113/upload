<?php
session_start();
if(!isset($_SESSION["phone"]))
{
    header('Location:create_resume.php');
}?>

<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style type="text/css">
.input
{
	border: 1px solid #DFDFDF;
    padding: 10px;
    width: 80%;
    font-size: 100%;
    margin-bottom: 18px;
}

label
{
	float: right;
    font-size: 18px;
    color: #191817bd;
    font-family: serif;
}	
table
{
	    width: 55%;
		    margin: 8% 2% 1% 18%;
}
#check
{
margin: 0% 0% 2% 33%;
}

#button
{
    background-color: mediumseagreen;
    border: none;
    color: white;
    padding: 9px 55px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    margin: 1% 46%;
    cursor: pointer;
}
</style>
<body>
<form method="POST" action="education.php">

<table>
	<tr>
		<td><label>Degree</label></td>
		<td><select name="degree" required class="input" >
                <option value=""></option>
                <option value="Secondary (10th) pass">Secondary (10th) pass</option>
                <option value="Higher(12th pass)">Higher(12th pass)</option>
                <option value="Diploma">Diploma</option>
                <option value="Bachelor degree">Bachelor degree</option>
                <option value="Master degree">Master degree</option>
                <option value="Doctorate">Doctorate</option>                
                <option value="Others">Others</option>
            </select><strong style="font-size: 20px;color: red;">  *</strong>
        </td>
	</tr>
	<tr>
		<td><label>College or University</label></td>
		<td><input type="text" name="college" class="input" required><strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>
	<tr>
		<td><label>Field of study</label></td>
		<td><input type="text" name="study" class="input" required><strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>
	<tr>
		<td><label>Time period</label></td>
		<td><label style="margin-left: 49px; float:left">From</label>
        <input type="date" name="from" class="select"/>
        <strong style="font-size: 20px;color: red;">  *</strong>
            
            To<input type="date" name="to" class="select"/>

            </select><strong style="font-size: 20px;color: red;">  *</strong>
            
            </td>
	</tr>
</table>
<input type="submit" name="next" id="button" value="Next"/>

</form>
</body>
</html>


<?php
extract($_POST);
include 'connect.php';
if(isset($next))
{
	$degree=$degree;
	$college=$college;
	$study=$study;
	$from=$from;
    $to=$to;
    $phone=$_SESSION["phone"];
	
	if($from>$to)
	{
			echo"<script type=\"text/javascript\">window.alert('Invalid course duration');</script>";

	}
	   $sql="UPDATE users SET degree='$degree',college='$college',field_study='$study',study_from='$from',study_to='$to' WHERE phone='$phone'";
	  
    if ($conn->query($sql) === TRUE) {
	echo"<script type=\"text/javascript\">window.location.href='/job_portal/status.php';</script>";
	}
	else
		{
		echo"<script type=\"text/javascript\">window.alert('Try again');
		window.location.href='/job_portal/education.php';</script>";
		}
}	
?>

