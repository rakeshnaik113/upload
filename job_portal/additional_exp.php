
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style type="text/css">
.input
{
	border: 1px solid #DFDFDF;
    padding: 10px;
    width: 80%;
    font-size: 100%;
    margin-bottom: 18px;
}

label
{
	float: right;
    font-size: 18px;
    color: #191817bd;
    font-family: serif;
}	
table
{
	    width: 55%;
		    margin: 8% 2% 1% 18%;
}

#button
{
    background-color: mediumseagreen;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    padding: 10px 35px;
    margin: 0% 0% 0% 41%;
    cursor: pointer;
}


.exp
{
	width: 61px;
    height: 29px;
    border-radius: 5px;
}

#check
{
margin: 0% 0% 2% 22%;
}

.add_button
{
	    background-color: mediumseagreen;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    margin: 0px 17px;
    cursor: pointer;
}
#add_button
{
	  background-color: mediumseagreen;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    padding: 10px 35px;
    cursor: pointer;
}
</style>
<body>
<form method="POST">

<table border="">
    <tr>
		<td><label>Job Tittle</label></td>
		<td><input type="text" name="job_title" class="input" required><strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>


    <tr>
		<td><label>Company</label></td>
		<td><input type="text" name="company" class="input" required><strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>

    <tr>
		<td><label>City</label></td>
		<td><input type="text" name="city" class="input" required><strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>
	
    <tr>
		<td><label>No.Experience year</label></td>
		<td><select name="month" required class="exp" >
				<option value=""></option>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
				<option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
				<option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
				<option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>Months
			
			<select name="year" required class="exp" >
                <option value=""></option>
				<option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
				<option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
				<option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
				<option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select> Years
			<strong style="font-size: 20px;color: red;"> *</strong></td>
	</tr>
			
</table>
</br>	
<span><input type="submit" name="next" id="button" value="save"></span>
	<span><button type="submit" name="add" class="add_button" />+</button><strong>Additional experience</strong>	<span>	
</form>
</body>
</html>