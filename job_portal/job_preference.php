<?php
session_start();
if(!isset($_SESSION["phone"]))
{
    header('Location:create_resume.php');
}
?>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style type="text/css">
.input
{
	border: 1px solid #DFDFDF;
    padding: 10px;
    width: 60%;
    font-size: 100%;
    margin-bottom: 18px;
}

label
{
	float: right;
    font-size: 18px;
    font-family: serif;
    margin-right: 13px;
}	
table
{
	    width: 45%;
		    margin: 8% 2% 1% 28%;
}

#button
{
    background-color: mediumseagreen;
    border: none;
    color: white;
    padding: 9px 55px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    margin: 1% 37%;
    cursor: pointer;
}
.salaryinput
{
	border: 1px solid #DFDFDF;
    padding: 10px;
    width: 30%;
    font-size: 100%;
    margin-bottom: 18px;
}

.salarymode
{
	border: 1px solid #DFDFDF;
    padding: 10px;
    width: 20%;
    font-size: 100%;
    margin-bottom: 18px;
}

</style>
<body>
<form method="POST">

<table>
    <tr>
		<td><label>Desire Job</label></td>
		<td><input type="text" name="desire_job" class="input" required><strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>


    <tr>
		<td><label>Job Type</label></td>
		<td><input type="radio" name="type" value="Full Time" required> Full Time Job
             <input type="radio" name="type" value="Part Time" required> Part Time Job<strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>


	<tr>
		<td><label>Desire Salary</label></td>
		<td><input type="text" name="salary" class="salaryinput" required><strong style="font-size: 20px;color: red;">  *</strong>
		mode of salary<select name="mode" class="salarymode"  required>
                <option value=""></option>
                <option value="Per Hour">Per Hour</option>
                <option value="per Day">per Day</option>
                <option value="Per Month">Per Month</option>
                <option value="Per Year">Per Year</option>
            </select><strong style="font-size: 20px;color: red;">  *</strong>
        </td>
	</tr>

    <tr>
		<td><label>Will to Relocate</label></td>
		<td> <input type="radio" name="relocate" value="Yes" required> Yes
             <input type="radio" name="relocate" value="No" required> No<strong style="font-size: 20px;color: red;">  *</strong></td>
	</tr>
</table>

<input type="submit" name="next" id="button" value="Next"/>

</form>
</body>
</html>




<?php
extract($_POST);
include 'connect.php';
if(isset($next))
{
	$desire_job=$desire_job;
	$type=$type;
	$salary=$salary;
	$mode=$mode;
	$relocate=$relocate;
	$phone=$_SESSION["phone"];
	
	
	   $sqlF="UPDATE users SET desire_job='$desire_job',job_type='$type',desire_salary='$salary',mode='$mode',relocate='$relocate' WHERE phone='$phone'";
	
    if ($conn->query($sqlF) === TRUE) {
	echo"<script type=\"text/javascript\">window.location.href='/job_portal/use_preference.php';</script>";
	}
	else
		{
		echo"<script type=\"text/javascript\">window.alert('Try again');
		window.location.href='/job_portal/job_preference.php';</script>";
		}
}	
?>